<?php

namespace Entri\Lib\Abstracts;

use function call_user_func_array;
use function get_called_class;

class AbstractSingleton
{
  private static array $instances = [];

  public function __get(string $key): mixed
  {
    return $this->{$key};
  }

  public static function __callStatic(string $method, array $arguments): mixed
  {
    return call_user_func_array([static::getInstance(), $method], $arguments);
  }

  public static function getInstance(): AbstractSingleton
  {
    $class = get_called_class();

    if (!isset(self::$instances[$class])) {
      self::$instances[$class] = new static();
    }

    return self::$instances[$class];
  }
}
