<?php

namespace Entri\Collection;

use Entri\Database\AbstractModel as Model;
use Entri\Support\Collection as BaseCollection;

use function array_push;
use function call_user_func;
use function count;
use function in_array;
use function method_exists;

class Collection extends BaseCollection
{
  protected array $relations;

  protected string $type;

  public function __construct(array $items, string $type, array $relations = [])
  {
    parent::__construct($items);

    $this->relations = $relations;
    $this->type = $type;

    $this->bootCollection();
    $this->bootCollectionRelations();
  }

  public function getRelations(): array
  {
    return $this->relations;
  }

  public function getType(): string
  {
    return $this->type;
  }

  public function hasRelations(): bool
  {
    return 0 !== count($this->getRelations());
  }

  public function bootCollection(): void
  {
    $this->each(fn($item) => $item->setExists(true));
  }

  public function bootCollectionRelations(): void
  {
    if (!$this->hasRelations()) {
      return;
    }

    $instance = $this->createTemporaryInstance();
    $instance = $this->callRelationMethods($instance);
    $this->attachRelationsToCurrent($instance);
  }

  public function attributesToArray(): array
  {
    $attributes = [];

    foreach ($this->getItems() as $item) {
      array_push($attributes, $item->toArray());
    }

    return $attributes;
  }

  public function except(array $keys): object
  {
    $filtered = $this->filter(function($model) use ($keys) {
      return !in_array($model->getPrimaryKeyValue(), $keys);
    });

    return new static($filtered->getItems(), $this->getType(), $this->getRelations());
  }

  public function extract(string $column): parent
  {
    $keys = [];

    foreach ($this->getItems() as $item) {
      array_push($keys, $item->getAttribute($column));
    }

    return new parent($keys);
  }

  public function load(array $relations): object
  {
    return new static($this->getItems(), $this->getType(), $relations);
  }

  public function filter(callable $callback): object
  {
    $result = parent::filter($callback);

    return new static($result->all(), $this->getType(), $this->getRelations());
  }

  public function makeHidden(array $columns): object
  {
    foreach ($this->getItems() as $item) {
      $item->makeHidden($columns);
    }

    return $this;
  }

  public function makeVisible(array $columns): object
  {
    foreach ($this->getItems() as $item) {
      $item->makeVisible($columns);
    }

    return $this;
  }

  public function map(callable $callback): object
  {
    $result = parent::map($callback);

    return new static($result->all(), $this->getType(), $this->getRelations());
  }

  public function modelKeys(): parent
  {
    $keys = [];

    foreach ($this->getItems() as $item) {
      array_push($keys, $item->getPrimaryKeyValue());
    }

    return new parent($keys);
  }

  public function only(array $keys): object
  {
    $filtered = $this->filter(function($model) use ($keys) {
      return in_array($model->getPrimaryKeyValue(), $keys);
    });

    return new static($filtered->getItems(), $this->getType(), $this->getRelation());
  }

  public function toArray(): array
  {
    return $this->attributesToArray();
  }

  protected function attachRelationsToCurrent(object $instance): void
  {
    $relations = $instance->getRelations();

    foreach ($relations as $key => $relation) {
      if ($relation instanceof Collection) {
        $this->attachCollectionToCurrent($instance, $key, $relation);
      }

      if ($relation instanceof Model) {
        $this->attachModelToCurrent($instance, $key, $relation);
      }
    }
  }

  protected function attachCollectionToCurrent(object $instance, mixed $key, object $relation): void
  {
    $relations = [];
    $class = $instance->getRelationClass($key);
    $foreign = $instance->getRelationKey($key);
    $pivot = $instance->getRelationPivot($key);
    $type = $instance->getRelationType();

    foreach ($relation as $subrelation) {
      $subrelationKey = $subrelation->getAttribute($foreign);

      if ($instance->usesRelationPivotTable($key)) {
        $subrelationKeys = $pivot[$foreign][$subrelationKey];

        foreach ($subrelationKeys as $subrelationKey) {
          $relations[$subrelationKey] = $relations[$subrelationKey] ?? [];

          array_push($relations[$subrelationKey], $subrelation);
        }
      } else {
        $relations[$subrelationKey] = $relations[$subrelationKey] ?? [];

        array_push($relations[$subrelationKey], $subrelation);
      }
    }

    $this->each(function($item) use ($class, $foreign, $instance, $key, $relations, $type): void {
      $relationsForItem = $relations[$item->getPrimaryKeyValue()] ?? [];
      $relation = new static($relationsForItem, $class);

      if (Model::$RELATION_SINGULAR === $type) {
        $relation = $relation->first();
        $relation = null === $relation ? new $class() : $relation;
      }

      $this->attachRelationToModel($item, $key, $relation, $class, $foreign);
    });
  }

  protected function attachModelToCurrent(object $instance, mixed $key, object $relation): void
  {
    $class = $instance->getRelationClass($key);
    $foreign = $instance->getRelationKey($key);

    $this->each(function($item) use ($class, $foreign, $key, $relation): void {
      if ($item->getPrimaryKeyValue() === $relation->getAttribute($foreign)) {
        $this->attachRelationToModel($item, $key, $relation, $class, $foreign);
      }
    });
  }

  protected function attachRelationToModel(object $model, mixed $key, mixed $relation, string $class, ?string $foreign): void
  {
    $model->setRelation($key, $relation);
    $model->setRelationClass($key, $class);
    $model->setRelationKey($key, $foreign);
  }

  protected function callRelationMethods(object $instance): object
  {
    foreach ($this->getRelations() as $relation) {
      if (method_exists($instance, $relation)) {
        call_user_func([$instance, $relation]);
      }
    }

    return $instance;
  }

  protected function createTemporaryInstance(): object
  {
    $instance = new ($this->getType());
    $instance->setAttribute($instance->getPrimaryKey(), $this->modelKeys()->all());
    $instance->setProxiedRequest(true);
    $instance->setProxiedOriginator($this);

    return $instance;
  }
}
