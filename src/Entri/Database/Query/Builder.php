<?php

namespace Entri\Database\Query;

use Entri\Collection\Collection;
use Entri\Support\Facades\DB;
use PDO;

use function is_object;
use function method_exists;

class Builder
{
  protected string $caller;

  protected mixed $query;

  protected array $relations;

  protected bool $singular = false;

  public function __construct(string $caller, mixed $query, array $relations = [])
  {
    $this->caller = $caller;
    $this->query = $query;
    $this->relations = $relations;
  }

  public function __call(string $method, array $arguments): mixed
  {
    if (method_exists($this->query, $method)) {
      $this->query = $this->query->{$method}(...$arguments);
    }

    return is_object($this->query) ? $this : $this->query;
  }

  public function avg(string $column): int
  {
    $this->query = $this->query->select("AVG({$column})", true);

    return $this->query->fetchColumn();
  }

  public function count(): int
  {
    return $this->query->count();
  }

  public function sum(string $column): int
  {
    $this->query = $this->query->select("SUM({$column})", true);

    return $this->query->fetchColumn();
  }

  public function max(string $column): int
  {
    $this->query = $this->query->select("MAX({$column})", true);

    return $this->query->fetchColumn();
  }

  public function min(string $column): int
  {
    $this->query = $this->query->select("MIN({$column})", true);

    return $this->query->fetchColumn();
  }

  public function first(): ?object
  {
    $this->query = $this->query->limit(1);
    $this->singular = true;

    return $this->get();
  }

  public function last(): ?object
  {
    $caller = new $this->caller();
    $primaryKey = $caller->getPrimaryKey();

    $this->query = $this->query->limit(1);
    $this->query = $this->query->orderBy("{$primaryKey} DESC");
    $this->singular = true;

    return $this->get();
  }

  public function get(): ?object
  {
    $query = DB::getPdo()->prepare($this->query);
    $query->execute($this->query->getParameters());
    $query->setFetchMode(PDO::FETCH_CLASS, $this->caller);
    $result = $query->fetchAll();
    $collection = new Collection($result, $this->caller, $this->relations);

    return $this->singular ? $collection->first() : $collection;
  }

  public function execute(): void
  {
    DB::convertWriteTypes(true);

    $query = DB::getPdo()->prepare($this->query);
    $query->execute($this->query->getParameters());
  }
}
