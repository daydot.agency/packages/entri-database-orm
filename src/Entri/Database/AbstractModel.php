<?php

namespace Entri\Database;

use Entri\Database\Query\Builder;
use Entri\Lib\Abstracts\AbstractSingleton;
use Entri\Support\Facades\DB;
use Entri\Support\Traits\HasAttributesTrait;
use Entri\Support\Traits\HasRelationshipsTrait;
use Entri\Support\Traits\HasTimestampsTrait;
use Entri\Support\Traits\ProxiesRelationshipsTrait;
use JsonSerializable;

use function array_merge;
use function call_user_func;
use function get_called_class;
use function json_encode;
use function method_exists;

use const JSON_NUMERIC_CHECK;

abstract class AbstractModel extends AbstractSingleton implements JsonSerializable
{
  use HasAttributesTrait;
  use HasRelationshipsTrait;
  use HasTimestampsTrait;
  use ProxiesRelationshipsTrait;

  protected const CREATED_AT = 'created_at';

  protected const UPDATED_AT = 'updated_at';

  protected bool $exists = false;

  protected string $primaryKey = 'id';

  protected string $table;

  public function __construct()
  {
    $this->syncOriginal();
  }

  public function __get(string $key): mixed
  {
    return $this->getAttribute($key);
  }

  public function __set(string $key, mixed $value): void
  {
    $this->setAttribute($key, $value);
  }

  public function getExists(): bool
  {
    return $this->exists;
  }

  public function setExists(bool $exists): void
  {
    $this->exists = $exists;
  }

  public function getPrimaryKey(): string
  {
    return $this->primaryKey;
  }

  public function getPrimaryKeyValue(): mixed
  {
    return $this->{$this->primaryKey};
  }

  public function getTable(): string
  {
    return $this->table;
  }

  public function toArray(): array
  {
    return array_merge($this->attributesToArray(), $this->relationsToArray());
  }

  public function toJson(): string
  {
    return json_encode($this->toArray(), JSON_NUMERIC_CHECK);
  }

  public function jsonSerialize(): array
  {
    return $this->toArray();
  }

  public function exists(): bool
  {
    return $this->getExists();
  }

  public function save(): void
  {
    if ($this->usesTimestamps()) {
      $this->updateTimestamps();
    }

    if ($this->exists()) {
      $this->performUpdate();
    }

    if (!$this->exists()) {
      $this->performInsert();
    }

    $this->setExists(true);
    $this->syncOriginal();
  }

  public function push(): void
  {
    $this->save();

    foreach ($this->getRelations() as $relation) {
      if (method_exists($relation, 'push')) {
        call_user_func([$relation, 'push']);
      }
    }
  }

  public function is(object $comparison): bool
  {
    if ($this->getTable() !== $comparison->getTable()) {
      return false;
    }

    if ($this->getPrimaryKeyValue() !== $comparison->getPrimaryKeyValue()) {
      return false;
    }

    return true;
  }

  protected function all(): mixed
  {
    $statement = DB::from($this->getTable());
    $query = new Builder(get_called_class(), $statement);

    return $query->get();
  }

  protected function find(int $id): mixed
  {
    $statement = DB::from($this->getTable())->where($this->getPrimaryKey(), $id);
    $query = new Builder(get_called_class(), $statement);

    return $query->first();
  }

  protected function select(array $columns = []): mixed
  {
    $statement = DB::from($this->getTable())->select($columns, true);

    return new Builder(get_called_class(), $statement);
  }

  protected function where(mixed ...$condition): mixed
  {
    $statement = DB::from($this->getTable())->where(...$condition);

    return new Builder(get_called_class(), $statement);
  }

  protected function with(array $relations): mixed
  {
    $statement = DB::from($this->getTable());

    return new Builder(get_called_class(), $statement, $relations);
  }

  protected function destroy(int $id): void
  {
    $statement = DB::deleteFrom($this->getTable())->where($this->getPrimaryKey(), $id);
    $query = new Builder(get_called_class(), $statement);
    $query->execute();
  }

  protected function performUpdate(): void
  {
    $statement = DB::update($this->getTable(), $this->getAttributes(), $this->getPrimaryKeyValue());
    $query = new Builder(get_called_class(), $statement);
    $query->execute();
  }

  protected function performInsert(): void
  {
    $statement = DB::select([$this->getPrimaryKey()]);
    $query = new Builder(get_called_class(), $statement);
    $result = $query->last();
    $lastSetPrimaryKey = null !== $result ? $result->getAttribute($this->getPrimaryKey()) : 0;

    $this->setAttribute($this->getPrimaryKey(), ($lastSetPrimaryKey + 1));

    $statement = DB::insertInto($this->getTable(), $this->getAttributes());
    $query = new Builder(get_called_class(), $statement);
    $query->execute();
  }
}
