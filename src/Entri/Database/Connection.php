<?php

namespace Entri\Database;

use Entri\Providers\DatabaseProvider;
use Envms\FluentPDO\Query as Fluent;
use PDO;

class Connection
{
  public Fluent $connection;

  public function addConnection(array $config): void
  {
    $pdo = new PDO("mysql:host={$config['host']};port={$config['port']};dbname={$config['database']}", $config['username'], $config['password']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

    $this->connection = new Fluent($pdo);
    DatabaseProvider::getInstance()->db = $this->connection;
  }
}
