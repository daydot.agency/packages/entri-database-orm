<?php

namespace Entri\Support\Traits;

use Carbon\Carbon;

use function array_key_exists;
use function array_keys;
use function array_merge;
use function array_search;
use function array_unique;
use function call_user_func;
use function call_user_func_array;
use function camel_case;
use function in_array;
use function intval;
use function is_numeric;
use function method_exists;
use function serialize;
use function studly_case;

trait HasAttributesTrait
{
  protected array $appends = [];

  protected array $attributes = [];

  protected array $dates = [];

  protected array $defaults = [];

  protected array $hidden = [];

  protected array $originals = [];

  public function getAppends(): array
  {
    return $this->appends;
  }

  public function getAppend(string $key): mixed
  {
    if (in_array($key, $this->getAppends())) {
      if (method_exists($this, 'get' . studly_case($key) . 'Attribute')) {
        return call_user_func([$this, 'get' . studly_case($key) . 'Attribute']);
      }
    }

    return null;
  }

  public function getAttributes(): array
  {
    return $this->attributes;
  }

  public function getAttribute(string $key, bool $clean = false): mixed
  {
    if (array_key_exists($key, $this->getAttributes())) {
      $attribute = $this->attributes[$key];

      if (method_exists($this, 'get' . studly_case($key) . 'Attribute') && !$clean) {
        return call_user_func_array([$this, 'get' . studly_case($key) . 'Attribute'], [$attribute]);
      }

      if (null !== $attribute && in_array($key, $this->getDates())) {
        return Carbon::parse($attribute);
      }

      return $attribute;
    }

    return $this->getRelationalAttribute($key);
  }

  public function setAttribute(string $key, mixed $value): void
  {
    if (method_exists($this, 'set' . studly_case($key) . 'Attribute')) {
      $value = call_user_func([$this, 'set' . studly_case($key) . 'Attribute'], $value);
    }

    $this->attributes[$key] = is_numeric($value) ? intval($value) : $value;
  }

  public function getDates(): array
  {
    if (!$this->usesTimestamps()) {
      return $this->dates;
    }

    $defaults = [
        $this->getCreatedAtColumn(),
        $this->getUpdatedAtColumn(),
    ];

    return array_unique(array_merge($this->dates, $defaults));
  }

  public function getDefaults(): array
  {
    return $this->defaults;
  }

  public function getDefault(string $key): mixed
  {
    return $this->defaults[$key] ?? null;
  }

  public function getHidden(): array
  {
    return $this->hidden;
  }

  public function setHidden(string $key): void
  {
    $this->hidden = array_merge($this->hidden, $key);
  }

  public function getOriginals(): array
  {
    return $this->originals;
  }

  public function getOriginal(string $key): mixed
  {
    return $this->originals[$key] ?? null;
  }

  public function getRelationalAttribute(string $key): mixed
  {
    if ($this->isRelationLoaded($key)) {
      return $this->relations[$key];
    }

    if (method_exists($this, camel_case($key))) {
      return call_user_func([$this, camel_case($key)]);
    }

    return null;
  }

  public function isRelationLoaded(string $key): bool
  {
    return null !== $this->getRelation($key);
  }

  public function syncOriginal(): void
  {
    $this->originals = $this->getAttributes();
  }

  public function attributesToArray(): array
  {
    $attributes = [];

    foreach (array_keys($this->getAttributes()) as $attribute) {
      $attributes[$attribute] = $this->getAttribute($attribute);
    }

    foreach ($this->getAppends() as $attribute) {
      $attributes[$attribute] = $this->getAppend($attribute);
    }

    foreach ($this->getHidden() as $attribute) {
      unset($attributes[$attribute]);
    }

    return $attributes;
  }

  public function isDirty(?string $key = null): bool
  {
    if (null !== $key) {
      if (null === $this->getOriginal($key)) {
        return true;
      }

      return serialize($this->getAttribute($key, true)) !== serialize($this->getOriginal($key));
    }

    return serialize($this->getAttributes()) !== serialize($this->getOriginals());
  }

  public function isClean(?string $key = null): bool
  {
    return !$this->isDirty($key);
  }

  public function makeHidden(array $columns): object
  {
    $this->hidden = array_merge($this->getHidden(), $columns);

    return $this;
  }

  public function makeVisible(array $columns): object
  {
    foreach ($columns as $column) {
      $key = array_search($column, $this->getHidden());

      if (false !== $key) {
        unset($this->hidden[$key]);
      }
    }

    return $this;
  }

  public function refresh(): void
  {
    $this->attributes = $this->getOriginal();
  }
}
