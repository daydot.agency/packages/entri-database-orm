<?php

namespace Entri\Support\Traits;

use Entri\Collection\Collection;
use Entri\Database\AbstractModel as Model;
use Entri\Database\Query\Builder;
use Entri\Support\Facades\DB;

use function array_push;
use function class_basename;
use function count;
use function debug_backtrace;
use function get_class;
use function is_array;
use function snake_case;

trait HasRelationshipsTrait
{
  public static string $RELATION_SINGULAR = 'singular';

  public static string $RELATION_MULTIPLE = 'multiple';

  protected array $relationClasses = [];

  protected array $relationKeys = [];

  protected array $relationPivots = [];

  protected string $relationType;

  protected array $relations = [];

  public function getRelationClasses(): array
  {
    return $this->relationClasses;
  }

  public function getRelationClass(string $key): ?string
  {
    return $this->relationClasses[$key] ?? null;
  }

  public function setRelationClass(string $key, mixed $value): void
  {
    $this->relationClasses[$key] = $value;
  }

  public function getRelationKeys(): array
  {
    return $this->relationKeys;
  }

  public function getRelationKey(string $key): ?string
  {
    return $this->relationKeys[$key] ?? null;
  }

  public function setRelationKey(string $key, mixed $value): void
  {
    $this->relationKeys[$key] = $value;
  }

  public function getRelationPivots(): array
  {
    return $this->relationPivots;
  }

  public function getRelationPivot(string $key): ?array
  {
    return $this->relationPivots[$key] ?? null;
  }

  public function setRelationPivot(string $key, mixed $value): void
  {
    $this->relationPivots[$key] = $value;
  }

  public function getRelationType(): ?string
  {
    return $this->relationType ?? null;
  }

  public function setRelationType(mixed $value): void
  {
    if ($value !== static::$RELATION_SINGULAR && $value !== static::$RELATION_MULTIPLE) {
      return;
    }

    $this->relationType = $value;
  }

  public function getRelations(): array
  {
    return $this->relations;
  }

  public function getRelation(string $key): mixed
  {
    return $this->relations[$key] ?? null;
  }

  public function setRelation(string $key, mixed $value): void
  {
    $this->relations[$key] = $value;
  }

  public function relationsToArray(): array
  {
    $attributes = [];

    foreach ($this->getRelations() as $key => $relation) {
      if ($relation instanceof Model) {
        $attributes[$key] = $relation->attributesToArray();
      }

      if ($relation instanceof Collection) {
        $attributes[$key] = [];

        foreach ($relation as $subrelation) {
          array_push($attributes[$key], $subrelation->attributesToArray());
        }
      }
    }

    return $attributes;
  }

  public function usesRelationPivotTable(?string $key = null): bool
  {
    if (null === $key) {
      return 0 !== count($this->getRelationPivots());
    }

    $specificPivotTable = $this->getRelationPivot($key);

    return null !== $specificPivotTable && 0 !== count($specificPivotTable);
  }

  protected function createBelongToRelationPivot(object $relation, string $connectionKey, string $relationKey): array
  {
    $pivotTable = [];
    $pivotTable[$relationKey] = [];

    foreach ($relation as $model) {
      $key = $model->getAttribute($connectionKey);
      $pivotTable[$relationKey][$key] = $pivotTable[$relationKey][$key] ?? [];
    }

    foreach ($relation as $model) {
      $key = $model->getAttribute($connectionKey);

      array_push($pivotTable[$relationKey][$key], $model->getPrimaryKeyValue());
    }

    return $pivotTable;
  }

  protected function createBelongToManyRelationPivot(object $pivot, object $relation, string $connectionKey, string $relationKey, string $relationPrimaryKey): array
  {
    $pivotTable = [];
    $pivotTable[$relationPrimaryKey] = [];

    foreach ($relation as $model) {
      $key = $model->getPrimaryKeyValue();
      $pivotTable[$relationPrimaryKey][$key] = $pivotTable[$relationPrimaryKey][$key] ?? [];
    }

    foreach ($pivot as $connection) {
      $key = $connection->getAttribute($connectionKey);

      array_push($pivotTable[$relationPrimaryKey][$key], $connection->getAttribute($relationKey));
    }

    return $pivotTable;
  }

  protected function createManyThroughRelationPivot(object $pivot, object $relation, string $connectionKey, string $relationKey): array
  {
    $pivotTable = [];
    $pivotTable[$relationKey] = [];

    if (0 === $relation->count()) {
      return $pivotTable;
    }

    foreach ($relation as $model) {
      $key = $model->getAttribute($relationKey);
      $pivotTable[$relationKey][$key] = $pivotTable[$relationKey][$key] ?? [];
    }

    foreach ($pivot as $model) {
      $key = $model->getPrimaryKeyValue();

      array_push($pivotTable[$relationKey][$key], $model->getAttribute($connectionKey));
    }

    return $pivotTable;
  }

  protected function determinePrimaryKeys(object $model, ?string $column = null): array
  {
    if ($model instanceof Collection) {
      return null === $column ? $model->modelKeys()->all() : $model->extract($column)->all();
    }

    $keys = $model->getPrimaryKeyValue();

    return is_array($keys) ? $keys : [$keys];
  }

  protected function belongsTo(string $relation, ?string $relationColumn = null): ?object
  {
    $instance = new $relation();
    $callingMethod = snake_case(debug_backtrace()[1]['function']);
    $foreignKey = null === $relationColumn ? (snake_case(class_basename($relation)) . '_id') : $relationColumn;
    $primaryKey = $this->determinePrimaryKeys($this->getProxiedOriginator(), $foreignKey);

    $statement = DB::from($instance->getTable())->where($instance->getPrimaryKey(), $primaryKey);
    $query = new Builder($relation, $statement);
    $result = $this->isProxiedRequest() ? $query->get() : $query->first();

    $this->setRelation($callingMethod, $result);
    $this->setRelationClass($callingMethod, $relation);
    $this->setRelationKey($callingMethod, $instance->getPrimaryKey());
    $this->setRelationPivot($callingMethod, $this->createBelongToRelationPivot($this->getProxiedOriginator(), $foreignKey, $instance->getPrimaryKey()));
    $this->setRelationType(self::$RELATION_SINGULAR);

    return $result;
  }

  protected function belongsToMany(string $relation, string $pivotTable, ?string $relationColumn = null, ?string $connectionColumn = null): ?object
  {
    $callingMethod = snake_case(debug_backtrace()[1]['function']);
    $relationKey = null === $relationColumn ? (snake_case(class_basename($this)) . '_id') : $relationColumn;
    $primaryKey = $this->determinePrimaryKeys($this);

    $statement = DB::from($pivotTable)->where($relationKey, $primaryKey);
    $query = new Builder(get_class($this), $statement);
    $pivot = $query->get();

    if (0 === $pivot->count()) {
      $emptyRelationInstance = new Collection([], $relation, []);
      $this->setRelation($callingMethod, $emptyRelationInstance);
      $this->setRelationClass($callingMethod, $relation);
      $this->setRelationType(self::$RELATION_MULTIPLE);

      return $emptyRelationInstance;
    }

    $instance = new $relation();
    $foreignKey = null === $connectionColumn ? (snake_case(class_basename($relation)) . '_id') : $connectionColumn;
    $primaryKey = $this->determinePrimaryKeys($pivot, $foreignKey);

    $statement = DB::from($instance->getTable())->where($instance->getPrimaryKey(), $primaryKey);
    $query = new Builder($relation, $statement);
    $result = $query->get();

    $this->setRelation($callingMethod, $result);
    $this->setRelationClass($callingMethod, $relation);
    $this->setRelationKey($callingMethod, $instance->getPrimaryKey());
    $this->setRelationPivot($callingMethod, $this->createBelongToManyRelationPivot($pivot, $result, $foreignKey, $relationKey, $instance->getPrimaryKey()));
    $this->setRelationType(self::$RELATION_MULTIPLE);

    return $result;
  }

  protected function hasOne(string $relation, ?string $relationColumn = null): ?object
  {
    $instance = new $relation();
    $callingMethod = snake_case(debug_backtrace()[1]['function']);
    $foreignKey = null === $relationColumn ? (snake_case(class_basename($this)) . '_id') : $relationColumn;
    $primaryKey = $this->determinePrimaryKeys($this);

    $statement = DB::from($instance->getTable())->where($foreignKey, $primaryKey);
    $query = new Builder($relation, $statement);
    $result = $this->isProxiedRequest() ? $query->get() : $query->first();

    $this->setRelation($callingMethod, $result);
    $this->setRelationClass($callingMethod, $relation);
    $this->setRelationKey($callingMethod, $foreignKey);
    $this->setRelationType(self::$RELATION_SINGULAR);

    return $result;
  }

  protected function hasOneThrough(string $relation, string $connection, ?string $relationColumn = null, ?string $connectionColumn = null): ?object
  {
    $callingMethod = snake_case(debug_backtrace()[1]['function']);
    $instance = new $connection();
    $foreignKey = null === $connectionColumn ? (snake_case(class_basename($this)) . '_id') : $connectionColumn;
    $primaryKey = $this->determinePrimaryKeys($this);

    $statement = DB::from($instance->getTable())->where($foreignKey, $primaryKey);
    $query = new Builder($connection, $statement);
    $result = $this->isProxiedRequest() ? $query->get() : $query->first();

    if (!$this->isProxiedRequest() && null === $result) {
      return null;
    }

    if ($this->isProxiedRequest() && 0 === $result->count()) {
      $emptyRelationInstance = new Collection([], $relation, []);
      $this->setRelation($callingMethod, $emptyRelationInstance);
      $this->setRelationClass($callingMethod, $relation);
      $this->setRelationType(self::$RELATION_SINGULAR);

      return $emptyRelationInstance;
    }

    $instance = new $relation();
    $foreignKey = null === $relationColumn ? (snake_case(class_basename($connection)) . '_id') : $relationColumn;
    $primaryKey = $this->determinePrimaryKeys($result);

    $statement = DB::from($instance->getTable())->where($foreignKey, $primaryKey);
    $query = new Builder($relation, $statement);
    $result = $this->isProxiedRequest() ? $query->get() : $query->first();

    $this->setRelation($callingMethod, $result);
    $this->setRelationClass($callingMethod, $relation);
    $this->setRelationKey($callingMethod, $foreignKey);
    $this->setRelationType(self::$RELATION_SINGULAR);

    return $result;
  }

  protected function hasMany(string $relation, ?string $relationColumn = null): ?object
  {
    $callingMethod = snake_case(debug_backtrace()[1]['function']);
    $instance = new $relation();
    $foreignKey = null === $relationColumn ? (snake_case(class_basename($this)) . '_id') : $relationColumn;
    $primaryKey = $this->determinePrimaryKeys($this);

    $statement = DB::from($instance->getTable())->where($foreignKey, $primaryKey);
    $query = new Builder($relation, $statement);
    $result = $query->get();

    $this->setRelation($callingMethod, $result);
    $this->setRelationClass($callingMethod, $relation);
    $this->setRelationKey($callingMethod, $foreignKey);
    $this->setRelationType(self::$RELATION_MULTIPLE);

    return $result;
  }

  protected function hasManyThrough(string $relation, string $connection, ?string $relationColumn = null, ?string $connectionColumn = null): ?object
  {
    $callingMethod = snake_case(debug_backtrace()[1]['function']);
    $instance = new $connection();
    $connectionKey = null === $connectionColumn ? (snake_case(class_basename($this)) . '_id') : $connectionColumn;
    $primaryKey = $this->determinePrimaryKeys($this);

    $statement = DB::from($instance->getTable())->where($connectionKey, $primaryKey);
    $query = new Builder($connection, $statement);
    $pivot = $query->get();

    if (0 === $pivot->count()) {
      $emptyRelationInstance = new Collection([], $relation, []);
      $this->setRelation($callingMethod, $emptyRelationInstance);
      $this->setRelationClass($callingMethod, $relation);
      $this->setRelationType(self::$RELATION_MULTIPLE);

      return $emptyRelationInstance;
    }

    $instance = new $relation();
    $foreignKey = null === $relationColumn ? (snake_case(class_basename($connection)) . '_id') : $relationColumn;
    $primaryKey = $this->determinePrimaryKeys($pivot);

    $statement = DB::from($instance->getTable())->where($foreignKey, $primaryKey);
    $query = new Builder($relation, $statement);
    $result = $query->get();

    $this->setRelation($callingMethod, $result);
    $this->setRelationClass($callingMethod, $relation);
    $this->setRelationKey($callingMethod, $foreignKey);
    $this->setRelationPivot($callingMethod, $this->createManyThroughRelationPivot($pivot, $result, $connectionKey, $foreignKey));
    $this->setRelationType(self::$RELATION_MULTIPLE);

    return $result;
  }
}
