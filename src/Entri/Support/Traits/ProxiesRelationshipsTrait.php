<?php

namespace Entri\Support\Traits;

use Entri\Collection\Collection;

trait ProxiesRelationshipsTrait
{
  protected Collection $proxiedOriginator;

  protected bool $proxiedRequest = false;

  public function getProxiedOriginator(): ?Collection
  {
    return $this->proxiedOriginator ?? null;
  }

  public function setProxiedOriginator(Collection $value): void
  {
    $this->proxiedOriginator = $value;
  }

  public function getProxiedRequest(): bool
  {
    return $this->proxiedRequest;
  }

  public function setProxiedRequest(bool $value): void
  {
    $this->proxiedRequest = $value;
  }

  public function isProxiedRequest(): bool
  {
    return $this->getProxiedRequest();
  }
}
