<?php

namespace Entri\Support\Traits;

use Carbon\Carbon;

trait HasTimestampsTrait
{
  protected bool $timestamps = true;

  public function getCreatedAtColumn(): string
  {
    return static::CREATED_AT;
  }

  public function setCreatedAt(mixed $value): void
  {
    $this->setAttribute($this->getCreatedAtColumn(), $value);
  }

  public function getUpdatedAtColumn(): string
  {
    return static::UPDATED_AT;
  }

  public function setUpdatedAt(mixed $value): void
  {
    $this->setAttribute($this->getUpdatedAtColumn(), $value);
  }

  public function usesTimestamps(): bool
  {
    return $this->timestamps;
  }

  protected function updateTimestamps(): void
  {
    $time = $this->freshTimestamp();
    $updatedAtColumn = $this->getUpdatedAtColumn();

    if ($this->isDirty()) {
      $this->setUpdatedAt($time);
    }

    $createdAtColumn = $this->getCreatedAtColumn();

    if (!$this->exists() && $this->isDirty()) {
      $this->setCreatedAt($time);
    }
  }

  public function freshTimestamp(): Carbon
  {
    return Carbon::now();
  }
}
