<?php

namespace Entri\Support\Facades;

use Entri\Providers\DatabaseProvider;

class DB
{
  public static function __callStatic(string $method, array $parameters = []): mixed
  {
    return DatabaseProvider::getInstance()->db->{$method}(...$parameters);
  }
}
