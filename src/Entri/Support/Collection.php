<?php

namespace Entri\Support;

use ArrayAccess;
use Countable;
use Iterator;
use JsonSerializable;

use function array_combine;
use function array_except;
use function array_filter;
use function array_keys;
use function array_map;
use function array_only;
use function array_search;
use function array_values;
use function count;
use function in_array;
use function json_encode;

use const ARRAY_FILTER_USE_BOTH;
use const JSON_NUMERIC_CHECK;

class Collection implements ArrayAccess, Countable, Iterator, JsonSerializable
{
  protected array $items;

  protected array $keys;

  protected int $position;

  public function __construct(array $items)
  {
    $this->items = $items;
    $this->keys = array_keys($this->items);
    $this->position = 0;
  }

  public static function make(array $items): object
  {
    return new static($items);
  }

  public function getItems(): array
  {
    return $this->items;
  }

  public function getItem(mixed $key): mixed
  {
    return $this->items[$key] ?? null;
  }

  public function setItem(mixed $key, mixed $value): void
  {
    if (null === $key) {
      $this->items[] = $value;

      return;
    }

    $this->items[$key] = $value;
  }

  public function getKeys(): array
  {
    return $this->keys;
  }

  public function setKeys(mixed $value): void
  {
    $this->keys = $value;
  }

  public function getKey(string $key): mixed
  {
    return $this->keys[$key] ?? null;
  }

  public function setKey(mixed $key, mixed $value): void
  {
    if (null === $key) {
      $this->keys[] = $value;

      return;
    }

    $this->keys[$key] = $value;
  }

  public function getPosition(): int
  {
    return $this->position;
  }

  public function setPosition(int $position): void
  {
    $this->position = $position;
  }

  public function all(): array
  {
    return $this->getItems();
  }

  public function count(): int
  {
    return count($this->getKeys());
  }

  public function current(): mixed
  {
    return $this->getItem($this->key());
  }

  public function each(callable $callback): object
  {
    foreach ($this->getItems() as $key => $item) {
      if ($callback($item, $key) === false) {
        break;
      }
    }

    return $this;
  }

  public function except(array $keys): object
  {
    return new static(array_except($this->getItems(), $keys));
  }

  public function filter(callable $callback): object
  {
    $filtered = array_filter($this->getItems(), $callback, ARRAY_FILTER_USE_BOTH);

    return new self($filtered);
  }

  public function first(): mixed
  {
    return $this->offsetGet(0);
  }

  public function jsonSerialize(): array
  {
    return $this->toArray();
  }

  public function key(): int
  {
    return $this->getKey($this->getPosition());
  }

  public function last(): mixed
  {
    return $this->offsetGet($this->count() - 1);
  }

  public function map(callable $callback): object
  {
    $keys = array_keys($this->getItems());
    $items = array_map($callback, $this->getItems(), $keys);

    return new self(array_combine($keys, $items));
  }

  public function offsetExists(mixed $offset): bool
  {
    return null !== $this->getItem($offset);
  }

  public function offsetGet(mixed $offset): mixed
  {
    return $this->getItem($offset);
  }

  public function offsetSet(mixed $offset, mixed $value): void
  {
    $this->setItem($offset, $value);

    if (null === $offset) {
      $this->setKey($offset, array_keys($this->getItems())[$this->count() - 1]);
    } else {
      if (!in_array($offset, $this->getKeys())) {
        $this->setKey(null, $offset);
      }
    }
  }

  public function offsetUnset(mixed $offset): void
  {
    unset($this->items[$offset]);
    unset($this->keys[array_search($offset, $this->getKeys())]);

    $this->setKeys(array_values($this->getKeys()));
  }

  public function next(): void
  {
    $this->setPosition($this->getPosition() + 1);
  }

  public function only(array $keys): object
  {
    return new static(array_only($this->getItems(), $keys));
  }

  public function rewind(): void
  {
    $this->setPosition(0);
  }

  public function toArray(): array
  {
    return $this->getItems();
  }

  public function toJson(): string
  {
    return json_encode($this->toArray(), JSON_NUMERIC_CHECK);
  }

  public function valid(): bool
  {
    return null !== $this->getKey($this->getPosition());
  }
}
