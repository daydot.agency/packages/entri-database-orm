<?php

namespace Entri\Providers;

use Entri\Lib\Abstracts\AbstractSingleton;
use Envms\FluentPDO\Query as Fluent;

class DatabaseProvider extends AbstractSingleton
{
  public Fluent $db;
}
